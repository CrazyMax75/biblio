package com.company;

public class Library {
    private String name_library;
    private String address;
    private String city;
    private String surname;

    public Library(String name_library, String address, String city, String surname) {
        this.name_library = name_library;
        this.address = address;
        this.city = city;
        this.surname = surname;
    }

    @Override
    public String toString() {
        return  "name_library=" + name_library + "\n" +
                "address=" + address + "\n" +
                "city=" + city + "\n" +
                "surname=" + surname;
    }
}
