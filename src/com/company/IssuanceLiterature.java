package com.company;

public class IssuanceLiterature extends Library{
    private String nameReadersHall;
    private String srname;
    private String nameLiterature;
    private String date;
    private String term;
    private int amount;

    public IssuanceLiterature(String name_library, String address, String city, String surname,String nameReadersHall,String srname,String nameLiterature,
                              String date,String term,int amount) {
        super(name_library, address, city, surname);
        this.nameReadersHall = nameReadersHall;
        this.srname = srname;
        this.nameLiterature = nameLiterature;
        this.date = date;
        this.term =term;
        this.amount =amount;
    }

    @Override
    public String toString() {
        return
                super.toString()+"\n"+ "nameReadersHall=" + nameReadersHall + "\n" +
                "srname=" + srname + "\n" +
                "nameLiterature=" + nameLiterature + "\n" +
                "date=" + date + "\n" +
                "term=" + term + "\n" +
                "amount=" + amount;
    }
}
