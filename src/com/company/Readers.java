package com.company;

public class Readers extends Library{
    private String suname;
    private String name;
    private String patronymic;
    private String placeWork;
    private int age;
    private String gender;

    public Readers(String name_library, String address, String city, String surname,String suname,String name,String patronymic,String placeWork,int age,String gender) {
        super(name_library, address, city, surname);
        this.suname = suname;
        this.name = name;
        this.patronymic = patronymic;
        this.placeWork = placeWork;
        this.age =age;
        this.gender =gender;
    }

    @Override
    public String toString() {
        return
                super.toString()+"\n"+
                 "suname=" + suname + "\n" +
                "name=" + name + "\n" +
                "patronymic=" + patronymic + "\n" +
                "placeWork=" + placeWork + "\n" +
                "age=" + age + "\n" +
                "gender=" + gender;
    }
}
