package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList <Library> commodityArrayList = new ArrayList<>();

        commodityArrayList.add(new ReadersHall("Алекснадрийская библиотека", "Александрия", "Александрия", "Македонский", "Читальный", 100,2,15));

        commodityArrayList.add(new Readers("Алекснадрийская библиотека", "Александрия", "Александрия", "Македонский", "Пушкин","Александр","Сергеевич","Училище",233,"Мужской"));

        commodityArrayList.add(new IssuanceLiterature("Алекснадрийская библиотека", "Александрия", "Александрия", "Македонский", "Читательский", "Пушкин", "Капитанская дочка", "10.03.1995","10 дней",1));

        for(Library com: commodityArrayList){
            System.out.println(com.toString());
        }
    }


}

