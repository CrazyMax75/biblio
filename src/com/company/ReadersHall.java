package com.company;

public class ReadersHall extends Library{
    private String nameHall;
    private int countSourceLiterature;
    private int floor;
    private int room;

    public ReadersHall(String name_library, String address, String city, String surname,String nameHall,int countSourceLiterature,int floor,int room) {
        super(name_library,address,city,surname);
        this.nameHall = nameHall;
        this.countSourceLiterature = countSourceLiterature;
        this.floor = floor;
        this.room = room;
    }

    @Override
    public String toString() {
        return super.toString()+"\n"+
                "nameHall=" + nameHall + "\n" +
                "countSourceLiterature=" + countSourceLiterature + "\n" +
                "floor=" + floor + "\n" +
                "room=" + room;
    }
}
